---
title: درباره من
date: 2019-05-29 17:19:03/
---

سلام من **ابوالفضلم** و الان که دارم این متن رو می‌نویسم ۲۰ ساله هستم. برنامه‌نویس بک‌اند و طراحِ رابط گرافیکم و تو پست [سلام دنیایی](https://abolfaazl.me/2019/05/29/hello-world/) که نوشتم میتونید بیشتر راجع حوزه‌ی شغلیم باهام آشنا بشید :)

یه دانشجوی نرم‌افزار از دانشگاهِ شهید شمسی پور تهران (روزانه) که یه دانشگاه فنی و دولتی هست و یه دوست برای همکارام...

من دوران ابتدایی خودم رو تو مدرسه‌ی ادب تهران گذروندم و دوران راهنماییم رو تو مدرسه‌ی نمونه دولتی شهید چیت چیان بودم، اول دبیرستان رو تو دبیرستان جابر بن حیان و مقطع هنرستان رو تو هنرستان توحید بودم و از اول ورود به دانشگاه هم (‌سال ۹۶ ) دانشکده شهید شمسی پور بودم که من رو کمتر میتونید اونجا ببینید :)))

من به شدت به حوزه‌ی گرافیک علاقه‌مندم و برنامه‌نویسی رو هم دوست دارم، همزمان فلسفه می‌خونم (کم) و شعر هم می‌نویسم(اونم کم) که بهم میگن باید یکیش رو انجام بدی، در کنار این‌ها علاقه‌ی زیادی به نوشتن دارم، وقتایی که هیچ توانِ روحی‌ای برام نمونده شروع می‌کنم به نوشتن و اوایل که می‌نوشتم و کاغذ پاره می‌کردم ولی الان به لطف یه سری از تکنولوژی‌ها فقط یه shift+delete میزنم :)

اخلاقیات و روحیات خاص خودم رو دارم، شوخ طبع شاید باشم، با اکثر آدما خیلی راحت ارتباط می‌گیرم دوستام و خانواده‌ام بهم میگن رازداری (ولی بعضی وقتا تو مسائل مربوط به خودم نمیتونم برای خودم رازدار خوبی باشم) در کنار همه‌ی اینا سعی می‌کنم وقتی که توی یه جمع که هستم خیلی سخت درونیات خودم رو بروز بدم و بیشتر سعی میکنم رو درونیات دیگرات تاثیر بزارم (‌با صحبت کردن و شوخی کردن و قس علی هذا :) ) 

یکسری از نزدیکام رو (‌شاید کمتر از انگشتان یک دست) که خیلی بهشون اعتماد دارم رو قرار دادم برای تخلیه‌ی روحی خودم، باهاشون درد و دل میکنم و ازشون راهنمایی و مشورت می‌گیرم.

الان مشغول کار روی یه استارت‌آپ هستم که سه تاییم و داریم با هم یاد می‌گیریم و در کنارش به صورت فریلنس کار می‌کنم و همچنین یه مسئولیت کوچیک برای بخش نرم‌افزار توی یه شرکت تجارتی دارم. تا قبل از اون هم تو استارتآپِ خفنِ دنگی‌پال بودم.

الان هم حس می‌کنم دیگه خیلی با من آشنا شدید و بسه دیگه :)‌

ممنونم که وقت گذاشتید و خوندید.

